package dao;

import junit.framework.TestCase;
import org.junit.Test;

public class UserDAOTest extends TestCase {

    @Test
    public void testGenerateSalt() {
        UserDAO dao = new UserDAO();
        assertFalse(dao.generateSalt().equals(dao.generateSalt()));
    }

    @Test
    public void testSamePasswordAndSalt() {
        UserDAO dao = new UserDAO();
        byte[] test = dao.generateSalt();
        String password1 = dao.hashPassword("hei", test);
        String password2 = dao.hashPassword("hei", test);

        assertEquals(password1, password2);
    }

    @Test
    public void testSamePasswordDifferntSalt() {
        UserDAO dao = new UserDAO();
        String password1 = dao.hashPassword("hei", dao.generateSalt());
        String password2 = dao.hashPassword("hei", dao.generateSalt());

        assertNotSame(password1, password2);
    }

    @Test
    public void testDifferntPasswordSameSalt() {
        UserDAO dao = new UserDAO();

        byte[] salt = dao.generateSalt();
        String password1 = dao.hashPassword("hei", salt);
        String password2 = dao.hashPassword("passord", salt);

        assertNotSame(password1, password2);
    }

}