import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+1";
        assertEquals("401", calculatorResource.calculate(expression));

        expression = " 300 - 99 - 1 ";
        assertEquals("200", calculatorResource.calculate(expression));

        expression = "4*5";
        assertEquals(20, calculatorResource.multiplication(expression));

        expression = "8/2/2";
        assertEquals(2, calculatorResource.division(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "4/2";
        assertEquals(2, calculatorResource.division(expression));

        expression = "8/2";
        assertEquals(4, calculatorResource.division(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "7*7";
        assertEquals(49, calculatorResource.multiplication(expression));

        expression = "4*5";
        assertEquals(20, calculatorResource.multiplication(expression));
    }
}
